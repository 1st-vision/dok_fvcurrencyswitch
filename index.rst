.. |label| replace:: Währungszeichen-Wechsler
.. |snippet| replace:: FvCurrencySwitch
.. |Author| replace:: 1st Vision GmbH
.. |minVersion| replace:: 5.3.0
.. |maxVersion| replace:: 5.5.4
.. |version| replace:: 1.0.0
.. |php| replace:: 7.2


|label|
============

.. sectnum::

.. contents:: Inhaltsverzeichnis



Überblick
---------
:Author: |Author|
:PHP: |php|
:Kürzel: |snippet|
:getestet für Shopware-Version: |minVersion| bis |maxVersion|
:Version: |version|

Beschreibung
------------
Das Plugin ermöglicht es Ihnen, das Währungssymbol des Kunden im Frontend abhängig von der Kundengruppe anzugeben.

Frontend
--------
Abhängig von der Nutzergruppe des Kunden wird das Währungssymbol wie im Backend angegeben geändert ausgegeben.

.. image:: FvCurrencySwitch1.png
.. image:: FvCurrencySwitch2.png
.. image:: FvCurrencySwitch3.png
.. image:: FvCurrencySwitch4.png

Backend
-------
Konfiguration
_____________
keine

Währungssymbol anpassen
__________________________
Im Freitextfeld unter den Nutzergruppen kann das Währungssymbol für diese Gruppe angepasst werden.
Hier steht neben den Währungen Euro und US-Dollar noch die Auswahl "Standard" zur Verfügung, durch welche das Währungszeichen nicht geändert wird.

.. image:: FvCurrencySwitch5.png
.. image:: FvCurrencySwitch6.png

Textbausteine
_____________

keine

Technische Beschreibung
------------------------
Um die Ersetzung des Währungssymbols zu ermöglichen würde der bisherige currency-modifier in Smarty ersetzt durch einen eigenen, welcher das Freitextfeld berücksichtigt.

Modifizierte Template-Dateien
-----------------------------
keine
